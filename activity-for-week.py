""" This script will retrieve and print GitLab activity for the week before a date, specifically
new issues created, merged MRs, and in-progress MRs.
"""

import os
import sys

import requests
import json
from datetime import datetime, timedelta


# Print query with line numbers
def print_with_line_numbers(query):
    for i, line in enumerate(query.splitlines()):
        print(f"{i + 1:3d} {line}")


def issues_created(project_full_path,
                   created_after,
                   created_before,
                   gitlab_api_endpoint,
                   gitlab_access_token):
    print('''#### New Issues

Review the priority of any new issues
''')

    created_after_formatted = created_after.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
    created_before_formatted = created_before.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    query = """
        query {
            project(fullPath: "%s") {
                issues(
                    createdAfter: "%s",
                    createdBefore: "%s"
                ) {
                    nodes {
                        iid
                        author {
                            username
                        }
                        title
                        createdAt
                        webUrl
                        labels {
                            nodes {
                                title
                            }
                        }
                    }
                }
            }
        }
    """ % (project_full_path, created_after_formatted, created_before_formatted)

    # print_with_line_numbers(query)

    headers = {
        "Authorization": f"Bearer {gitlab_access_token}",
        "Content-Type": "application/json",
    }

    response = requests.post(gitlab_api_endpoint, headers=headers, json={"query": query})

    if response.status_code == 200:
        response_content = json.loads(response.content)
        if 'data' not in response_content and 'errors' in response_content:
            print("Errors received while retrieving issues:", response_content['errors'])
        else:
            # pprint(response_content)
            issues_data = response_content["data"]["project"]["issues"]["nodes"]

            issues_data_sorted = sorted(issues_data, key=lambda x: x["createdAt"])
            for issue in issues_data_sorted:
                issue_number = issue["iid"]
                issue_title = issue["title"]
                issue_author_username = issue["author"]["username"]
                issue_created_at = datetime.strptime(issue["createdAt"], '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%d')
                issue_url = issue["webUrl"]
                issue_link = f'''[#{issue_number}: "{issue_title}"]({issue_url}) (created: {issue_created_at})'''
                issue_labels = issue["labels"]["nodes"]
                print(f"- {issue_link}")
                print(f"  - Author: @{issue_author_username}")
                if issue_labels:
                    label_list = ", ".join([f'''~"{label['title']}"''' for label in issue_labels])
                    print(f"  - Labels: {label_list}")
    else:
        print("Error retrieving issues:", response.content)


def merged_mrs(project_full_path, merged_after, merged_before, gitlab_api_endpoint, gitlab_access_token):
    print('#### Merged MRs\n')
    query = """
        query {
            project(fullPath: "%s") {
                mergeRequests(
                    mergedAfter: "%s",
                    mergedBefore: "%s"
                ) {
                    nodes {
                        iid
                        title
                        author {
                            username
                        }
                        createdAt
                        mergedAt
                        webUrl
                        milestone {
                            title
                        }
                    }
                }
            }
        }
    """ % (project_full_path, merged_after, merged_before)

    # print_with_line_numbers(query)

    headers = {
        "Authorization": f"Bearer {gitlab_access_token}",
        "Content-Type": "application/json",
    }

    response = requests.post(gitlab_api_endpoint, headers=headers, json={"query": query})

    if response.status_code == 200:
        response_content = json.loads(response.content)
        if 'data' not in response_content and 'errors' in response_content:
            print("Errors received while retrieving merge requests:", response_content['errors'])
            return

        # pprint(response_content)
        merge_requests_data = response_content["data"]["project"]["mergeRequests"]["nodes"]

        merge_requests_data_sorted = sorted(merge_requests_data, key=lambda x: x["mergedAt"])
        for merge_request in merge_requests_data_sorted:
            merge_request_number = merge_request["iid"]
            merge_request_title = merge_request["title"]
            merge_request_author_username = merge_request["author"]["username"]
            merge_request_merged_at = datetime.strptime(merge_request["mergedAt"], '%Y-%m-%dT%H:%M:%SZ').strftime(
                '%Y-%m-%d')

            merge_request_url = merge_request["webUrl"]
            merge_request_link = f'''[!{merge_request_number}: "{merge_request_title}"]({merge_request_url}) (merged: {merge_request_merged_at})'''
            print(f"- {merge_request_link}")
            print(f"  - Author: @{merge_request_author_username}")


def open_mrs(project_full_path, after, before, gitlab_api_endpoint, gitlab_access_token):
    print('#### In-progress MRs and next steps of each\n')
    query = """
        query {
            project(fullPath: "%s") {
                mergeRequests(
                    state: opened,
                ) {
                    nodes {
                        iid
                        title
                        author {
                            username
                        }
                        createdAt
                        mergedAt
                        webUrl
                        milestone {
                            title
                        }
                    }
                }
            }
        }
    """ % (project_full_path)

    # print_with_line_numbers(query)

    headers = {
        "Authorization": f"Bearer {gitlab_access_token}",
        "Content-Type": "application/json",
    }

    response = requests.post(gitlab_api_endpoint, headers=headers, json={"query": query})

    if response.status_code == 200:
        response_content = json.loads(response.content)
        if 'data' not in response_content and 'errors' in response_content:
            print("Errors received while retrieving merge requests:", response_content['errors'])
            return

        # pprint(response_content)
        merge_requests_data = response_content["data"]["project"]["mergeRequests"]["nodes"]

        merge_requests_data_sorted = sorted(merge_requests_data, key=lambda x: x["createdAt"])
        for merge_request in merge_requests_data_sorted:
            merge_request_number = merge_request["iid"]
            merge_request_title = merge_request["title"]
            merge_request_author_username = merge_request["author"]["username"]
            merge_request_created_at = datetime.strptime(merge_request["createdAt"], '%Y-%m-%dT%H:%M:%SZ').strftime(
                '%Y-%m-%d')

            merge_request_url = merge_request["webUrl"]
            merge_request_link = f'''[!{merge_request_number}: "{merge_request_title}"]({merge_request_url}) (created: {merge_request_created_at})'''
            print(f"- {merge_request_link}")
            print(f"  - Author: @{merge_request_author_username}")
            print('  - Next step?')


def mr_review_time(project_full_path, after, before):
    base_url = f'https://gitlab.com/{project_full_path}'
    mr_dashboard_url = f'{base_url}/insights/#/mergeRequests'
    after_str = after.strftime('%Y-%m-%d')
    before_str = before.strftime('%Y-%m-%d')
    mr_analytics_url = f'{base_url}/-/analytics/merge_request_analytics?start_date={after_str}&end_date={before_str}'

    print(f'''#### How long are MRs taking to review, and is this okay?

[MR Insights Dashboard]({mr_dashboard_url})

[MR Analytics]({mr_analytics_url})
            ''')


def main():
    # Ensure that the date is passed as an argument
    if len(sys.argv) < 2:
        print("Please pass a date in the format YYYY-MM-DD")
        sys.exit(1)
    gitlab_api_endpoint = "https://gitlab.com/api/graphql"
    gitlab_access_token = os.environ.get("GRAPHQL_TOKEN", "")
    project_full_path = os.environ.get("PROJECT_FULL_PATH", "")
    # Ensure that the environment variables are set
    if not gitlab_access_token or not project_full_path:
        print("Please set the environment variables GRAPHQL_TOKEN and PROJECT_FULL_PATH")
        sys.exit(1)

    # Read the date from the command line
    before_string = sys.argv[1]
    # Convert created_before to a datetime object
    before = datetime.strptime(before_string, '%Y-%m-%d')
    # created_after is one week before created_before
    after = before - timedelta(weeks=1)

    # Print input parameters
    print("Project full path:", project_full_path)
    print("After:", after)
    print("Before:", before)

    print(f'''## {before.strftime('%Y-%m-%d')}

### Roll call

### What has happened since last meeting?

<https://gitlab.com/exosphere/exosphere/activity>''')

    print('')
    issues_created(project_full_path, after, before, gitlab_api_endpoint, gitlab_access_token)

    print('')
    merged_mrs(project_full_path, after, before, gitlab_api_endpoint, gitlab_access_token)

    print('')
    open_mrs(project_full_path, after, before, gitlab_api_endpoint, gitlab_access_token)

    print('')
    mr_review_time(project_full_path, after, before)

    print('''### Review prioritized issues

- [ ] Discuss [Priority::1 issues](https://gitlab.com/exosphere/exosphere/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Priority%3A%3A1)
- [ ] On first meeting of every month, discuss [Priority::2 issues](https://gitlab.com/exosphere/exosphere/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Priority%3A%3A2)

### Reminder: update Exosphere Collaborator calendar with your out-of-office plans.

### Open Discussion

---
''')


if __name__ == "__main__":
    main()
