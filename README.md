# Utility Scripts for Exosphere Project

## Getting starated

[Install pipenv](https://pipenv.pypa.io/en/latest/installation/).

Create the pipenv project: 

```bash
pipenv install
```

Copy `.env.example` to `.env` and populate with the following values:

```
GRAPHQL_TOKEN=glpat-YOUR-OWN-TOKEN
PROJECT_FULL_PATH="exosphere/exosphere"
```

## Generate Weekly Meeting Notes

This will generate meeting notes by querying GitLab for issues and merge
requests from the last seven days:

```bash
pipenv run python activity-for-week.py $(date +%Y-%m-%d)
```

